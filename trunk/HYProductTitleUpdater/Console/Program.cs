﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYProductTitleUpdater;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var controller = new Controller();
            controller.Start();
        }
    }
}
