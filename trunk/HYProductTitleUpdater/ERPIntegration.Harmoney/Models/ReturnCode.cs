﻿namespace ERPIntegration.Harmoney.Models
{
    public enum ReturnCode
    {
        NotProcessed,
        Failure,
        Success,
        Maintenance,
        CommunicationError
    }
}