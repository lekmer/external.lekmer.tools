﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace ERPIntegration.Harmoney.Models
{
    public class HarmoneyWebServiceResponse
    {
        public ReturnCode ReturnCode { get; set; }
        public XmlNode ResponseNode { get; set; }
        public string InXml { get; set; } 
    }
}
