﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using ERPIntegration.Harmoney.Contracts;
using ERPIntegration.Harmoney.Models;
using ERPIntegration.Harmoney.se.pulsen.webservice;

namespace ERPIntegration.Harmoney
{
    public class HarmoneyWebService : IHarmoneyWebService
    {
        public HarmoneyWebServiceResponse SendOrder(string orderNumber, string xml, string webServiceUrl)
        {
            throw new NotImplementedException();
        }

        public HarmoneyWebServiceResponse SendProduct(string productNumber, string xml, string webServiceUrl)
        {
            var timeout = TimeSpan.FromSeconds(30);

            using (var client = new PxxW1050 { Url = webServiceUrl, Timeout = (int)timeout.TotalMilliseconds })
            {
                return GetResponse(() => client.Beskrivning(xml));
            }
        }

        public HarmoneyWebServiceResponse SendCustomer(string customerNumber, string xml, string webServiceUrl)
        {
            throw new NotImplementedException();
        }

        public HarmoneyWebServiceResponse SendSupplier(string supplierNumber, string xml, string webServiceUrl)
        {
            throw new NotImplementedException();
        }

        private static HarmoneyWebServiceResponse GetResponse(Func<XmlNode> executor)
        {
            XmlNode response;
            try
            {
                response = executor();
            }
            catch (WebException)
            {
                return new HarmoneyWebServiceResponse { ReturnCode = ReturnCode.CommunicationError };
            }
            catch (Exception)
            {
                return new HarmoneyWebServiceResponse { ReturnCode = ReturnCode.Failure };
            }

            ReturnCode returnCode;
            return new HarmoneyWebServiceResponse
            {
                ReturnCode = TryGetWorstStatusCode(response, out returnCode) ? returnCode : ReturnCode.Failure,
                ResponseNode = response
            };
        }

        public static bool TryGetWorstStatusCode(XmlNode response, out ReturnCode returnCode)
        {
            returnCode = ReturnCode.Failure;
            XDocument xResponse = XDocument.Parse(response.OuterXml);
            var statusCodes = from s in
                                  (from r in xResponse.Descendants("Sys")
                                   where r.Descendants("Sta") != null
                                   select r).Descendants("Sta")
                              select s.Value;

            var returnCodesRanked = new List<ReturnCodeRanked>
            {
                new ReturnCodeRanked(0, "99", ReturnCode.Failure),
                new ReturnCodeRanked(1, "97", ReturnCode.Maintenance),
                new ReturnCodeRanked(2, "03", ReturnCode.Failure),
                new ReturnCodeRanked(3, "02", ReturnCode.Failure),
                new ReturnCodeRanked(4, "98", ReturnCode.Success),
                new ReturnCodeRanked(5, "01", ReturnCode.Success),
                new ReturnCodeRanked(6, "00", ReturnCode.Success)
            };

            var worstStatusCodes = from sc in statusCodes
                                   join rcr in returnCodesRanked on sc equals rcr.Value
                                   select new { rcr.Code, rcr.Rank };

            if (worstStatusCodes.Count() == 0)
            {
                return false;
            }

            returnCode = (from wsc in worstStatusCodes
                          orderby wsc.Rank ascending
                          select wsc.Code).First();

            return true;
        }

        private class ReturnCodeRanked
        {
            private readonly int _rank;
            private readonly string _value;
            private readonly ReturnCode _returnCode;

            public ReturnCodeRanked(int rank, string value, ReturnCode returnCode)
            {
                _rank = rank;
                _value = value;
                _returnCode = returnCode;
            }

            public ReturnCode Code
            {
                get { return _returnCode; }
            }

            public int Rank
            {
                get { return _rank; }
            }

            public string Value
            {
                get { return _value; }
            }
        }
    }
}
