﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ERPIntegration.Harmoney.Models;

namespace ERPIntegration.Harmoney.Contracts
{
    public interface IHarmoneyWebService
    {
        HarmoneyWebServiceResponse SendOrder(string orderNumber, string xml, string webServiceUrl);
        HarmoneyWebServiceResponse SendProduct(string productNumber, string xml, string webServiceUrl);
        HarmoneyWebServiceResponse SendCustomer(string customerNumber, string xml, string webServiceUrl);
        HarmoneyWebServiceResponse SendSupplier(string supplierNumber, string xml, string webServiceUrl);     
    }
}
