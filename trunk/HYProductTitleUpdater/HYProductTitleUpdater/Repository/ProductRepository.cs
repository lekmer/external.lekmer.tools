﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ERPIntegration.Harmoney.Models;
using HYProductTitleUpdater.Contracts;
using HYProductTitleUpdater.Models;
using log4net;
using log4net.Config;
using System.Linq; 

namespace HYProductTitleUpdater.Repository
{
    public class ProductRepository
    {
        private readonly ICoreConfiguration _configuration;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ProductRepository));

        public ProductRepository(ICoreConfiguration configuration)
        {
            _configuration = configuration;
            XmlConfigurator.Configure(); // log4net configuration
        }

        public List<IArticle> GetArticles(string languageIso)
        {
            var articleList = new List<IArticle>();

            //var t = from art in articleList
            //        where art.ChannelId == 1
            //        select art;

            using (var cnn = new SqlConnection(_configuration.DatabaseConfiguration.ConnectionString))
            {
                var cmd = new SqlCommand("[integration].[pGetProductTitlesForUpdateInHY]", cnn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "[integration].[pGetProductTitlesForUpdateInHY]",
                    CommandTimeout = int.Parse(_configuration.DatabaseConfiguration.Timeout)
                };

                cmd.Parameters.Add("@ISO", SqlDbType.NVarChar).Value = languageIso;

                try
                {
                    cnn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Logger.Info("Reading Articles from DB");
                        var tmpArticle = new Article
                            {
                                HyId = reader["HYErpId"] != DBNull.Value? reader.GetString(reader.GetOrdinal("HYErpId")): string.Empty,
                                ProductId = reader.GetInt32(reader.GetOrdinal("ProductId")),
                                LanguageId = reader.GetInt32(reader.GetOrdinal("LanguageId")),
                                Title = reader.GetString(reader.GetOrdinal("Title")),
                                ChannelId = reader.GetInt32(reader.GetOrdinal("ChannelId")),
                                Fok = "00" + reader.GetInt32(reader.GetOrdinal("ChannelId"))
                            };
                        
                        string[] articleId = tmpArticle.HyId.Split('-');
                        tmpArticle.HyId = articleId[0];
                        tmpArticle.Title = tmpArticle.Title.Replace(";", "");

                        articleList.Add(tmpArticle);
                    }                 

                    return articleList;
                }
                catch (Exception e)
                {
                    Logger.Error(string.Format("process failed:\nMessage: {0}\nStacktrace: {1}", e.Message, e.StackTrace));
                    return null;
                }
            }            
        }

        public void UpdateArticleStatus(IArticle article, ReturnCode returnCode)
        {
            using (var cnn = new SqlConnection(_configuration.DatabaseConfiguration.ConnectionString))
            {
                var cmd = new SqlCommand("[integration].[pUpdateProductTitlesInHY]", cnn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "[integration].[pUpdateProductTitlesInHY]",
                    CommandTimeout = int.Parse(_configuration.DatabaseConfiguration.Timeout)
                };

                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = article.ProductId;
                cmd.Parameters.Add("@LanguageId", SqlDbType.Int).Value = article.LanguageId;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = article.Title;
                cmd.Parameters.Add("@SuccessfulUpdate", SqlDbType.Bit).Value = returnCode != ReturnCode.Success ? 0 : 1;

                try
                {
                    cnn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Logger.Error(string.Format("process failed:\nMessage: {0}\nStacktrace: {1}", e.Message, e.StackTrace));
                }
            }
        }      
    }
}
