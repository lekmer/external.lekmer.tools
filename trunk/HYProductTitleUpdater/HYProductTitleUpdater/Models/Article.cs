﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYProductTitleUpdater.Contracts;

namespace HYProductTitleUpdater.Models
{
    public class Article : IArticle
    {
        public string HyId { get; set; }
        public int ProductId { get; set; }
        public int LanguageId { get; set; }
        public string Title { get; set; }
        public int ChannelId { get; set; }
        public string Fok { get; set; }       
    }
}
