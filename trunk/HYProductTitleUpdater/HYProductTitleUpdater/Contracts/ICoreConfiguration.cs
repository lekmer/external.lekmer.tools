﻿namespace HYProductTitleUpdater.Contracts
{
    public interface ICoreConfiguration
    {
        string WebServiceUrl { get; }
        string DatabaseSection { get; }
        IDatabaseConfiguration DatabaseConfiguration { get; }        
    }
}
