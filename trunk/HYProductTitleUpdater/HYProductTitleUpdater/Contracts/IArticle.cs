﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYProductTitleUpdater.Contracts
{
    public interface IArticle
    {
        string HyId { get; set; }
        int ProductId { get; set; }
        int LanguageId { get; set; }
        string Title { get; set; }
        int ChannelId { get; set; }
        string Fok { get; set; }    
    }
}
