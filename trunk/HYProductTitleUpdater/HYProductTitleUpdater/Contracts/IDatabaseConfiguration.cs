﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYProductTitleUpdater.Contracts
{
    public interface IDatabaseConfiguration
    {
        string Timeout { get; }
        string ConnectionString { get; }
    }
}
