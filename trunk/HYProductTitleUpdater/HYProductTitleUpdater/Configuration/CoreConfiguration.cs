﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using HYProductTitleUpdater.Contracts;

namespace HYProductTitleUpdater.Configuration
{
    class CoreConfiguration : ConfigurationSection, ICoreConfiguration
    {
        [ConfigurationProperty("webServiceUrl", IsRequired = true)]
        public string WebServiceUrl
        {
            get { return (string)this["webServiceUrl"]; }
        }      

        [ConfigurationProperty("databaseConfigurationSection", IsRequired = true)]
        public string DatabaseSection
        {
            get { return (string)this["databaseConfigurationSection"]; }
        }        

        public IDatabaseConfiguration DatabaseConfiguration
        {
            get { return (DatabaseConfiguration)ConfigurationManager.GetSection(DatabaseSection); }
        }
       
    }
}
