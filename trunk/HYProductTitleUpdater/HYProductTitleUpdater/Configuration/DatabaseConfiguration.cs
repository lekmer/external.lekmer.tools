﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using HYProductTitleUpdater.Contracts;

namespace HYProductTitleUpdater.Configuration
{
    class DatabaseConfiguration : ConfigurationSection, IDatabaseConfiguration
    {
        [ConfigurationProperty("timeout", IsRequired = true)]
        public string Timeout
        {
            get { return (string)this["timeout"]; }
        }

        [ConfigurationProperty("connectionString", IsRequired = true)]
        public string ConnectionString
        {
            get { return (string)this["connectionString"]; }
        }      
    }
}
