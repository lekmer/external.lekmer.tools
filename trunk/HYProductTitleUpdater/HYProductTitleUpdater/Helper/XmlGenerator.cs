﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYProductTitleUpdater.Contracts;
using HYProductTitleUpdater.Repository;
using log4net;
using log4net.Config;

namespace HYProductTitleUpdater.Helper
{
    public class XmlGenerator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XmlGenerator));

        public XmlGenerator()
        {
            XmlConfigurator.Configure();
        }

        public string GenerateXml(IArticle article, string languageIso)
        {
            try
            {
                var xml = new StringBuilder();
                string titel = article.Title;
                if (article.Title.Length > 75) { titel = article.Title.Substring(0, 75); }

                xml
                    .Append("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\r\n")
                    .Append("		<Data>\r\n")
                    .Append("		    <Sty>\r\n")
                    .Append("		        <Sys>PLE</Sys>\r\n")
                    .Append("		        <Anv>INET</Anv>\r\n")
                    .Append("		        <Losen>lekmer20</Losen>\r\n")
                    .Append("		        <Fok>" + article.Fok + "</Fok>\r\n")
                    .Append("		        <Funk>02</Funk>\r\n")
                    .Append("		    </Sty>\r\n")
                    .Append("		    <Art B=\"U\">\r\n")
                    .Append("		        <ArtNr>" + article.HyId + "</ArtNr>\r\n")
                    .Append("		        <BenTab B=\"U\">\r\n")
                    .Append("		                <Fok>" + article.Fok + "</Fok>\r\n")
                    .Append("		                <Sprak>" + languageIso + "</Sprak>\r\n")
                    .Append("		                <KortBen>" + titel.Substring(0, 23) + "</KortBen>\r\n")
                    .Append("		                <Ben>" + titel + "</Ben>\r\n")
                    .Append("		        </BenTab>\r\n")
                    .Append("		    </Art>\r\n")
                    .Append("       </Data>\r\n").ToString();


                Logger.Info("Xml generated for article: " + "(HYId: " + article.HyId + " )" + "(ProductId: " + article.ProductId + " )");
                return xml.ToString();
            }
            catch (Exception e)
            {
                Logger.Error("Problem generating xml for article:" + article.HyId + " Exception: " + e.Message);
                return null;
            }
        }
    }
}
