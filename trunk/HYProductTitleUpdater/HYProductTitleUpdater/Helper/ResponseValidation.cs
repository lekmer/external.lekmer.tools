﻿using System;
using ERPIntegration.Harmoney.Models;
using HYProductTitleUpdater.Contracts;
using HYProductTitleUpdater.Repository;
using log4net;
using log4net.Config;

namespace HYProductTitleUpdater.Helper
{
    public class ResponseValidation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ResponseValidation));

        public ResponseValidation()
        {
            XmlConfigurator.Configure();
        }

        public void ProcessReturnCode(ReturnCode returnCode, IArticle article, ProductRepository repository) 
        {
            try
            {
                switch (returnCode)
                {
                    case ReturnCode.NotProcessed:
                        Logger.Error("Article NotProcessed " + article.ProductId);                        
                        break;
                    case ReturnCode.Failure:                        
                        Logger.Error("Failure " + article.ProductId);
                        repository.UpdateArticleStatus(article, returnCode);
                        break;
                    case ReturnCode.Success:
                        Logger.Info("Success");
                        repository.UpdateArticleStatus(article, returnCode);
                        break;
                    case ReturnCode.Maintenance:
                        Logger.Error("Maintenance " + article.ProductId);
                        break;
                    case ReturnCode.CommunicationError:
                        Logger.Error("CommunicationError " + article.ProductId);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format("process failed:\nMessage: {0}\nStacktrace: {1}", e.Message, e.StackTrace));
            }
        } 
    }
}
