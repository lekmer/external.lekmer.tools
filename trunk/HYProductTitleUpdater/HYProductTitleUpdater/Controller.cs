﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ERPIntegration.Harmoney;
using ERPIntegration.Harmoney.Models;
using HYProductTitleUpdater.Configuration;
using HYProductTitleUpdater.Contracts;
using HYProductTitleUpdater.Helper;
using HYProductTitleUpdater.Repository;

namespace HYProductTitleUpdater
{
    public class Controller
    {
        public void Start()
        {
            ICoreConfiguration config = (CoreConfiguration) ConfigurationManager.GetSection("lekmerConfiguration");
            var repository = new ProductRepository(config);
            var xmlGenerator = new XmlGenerator();
            var harmoneyWebService = new HarmoneyWebService();
            var responseValidation = new ResponseValidation();


            foreach (var languageIso in new List<string> { "FI", "NO", "DA", "SV" })
            {
                foreach (var article in repository.GetArticles(languageIso))
                {                    
                    var xml = xmlGenerator.GenerateXml(article, languageIso);
                    var returnCode = harmoneyWebService.SendProduct(article.HyId, xml, config.WebServiceUrl).ReturnCode;

                    Console.WriteLine(returnCode == ReturnCode.Failure ? 
                        article.HyId + "[" + languageIso + "] FAILED: Product does not exist in Harmoney"
                        : article.HyId + "[" + languageIso + "] SUCCESS: " + article.Title);

                    responseValidation.ProcessReturnCode(returnCode, article, repository);
                }
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
