﻿USE [Lekmer_212]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [integration].[pUpdateProductTitlesInHY] -- delete [pTranslateProductTitlesInHY]
	@ProductId			int,
	@LanguageId			int,
	@Title				nvarchar(250),
	@SuccessfulUpdate	bit
AS
begin
	set nocount on
	begin try
		
		declare @Exists bit
		set @Exists = (select 1 from [integration].[tProductTitlesUpdatedInHY] where ProductId = @ProductId and LanguageId = @LanguageId)
	
		begin transaction
	
			if @SuccessfulUpdate = 1 and @Exists = 1
			begin
			
				Update
					[integration].[tProductTitlesUpdatedInHY]
				set
					Title = @Title,
					InsertedDate = GETDATE()
				where
					ProductId = @ProductId
					and LanguageId = @LanguageId
					
			end	
			else if @SuccessfulUpdate = 1 and @Exists is null
			begin	
			
				insert into [integration].[tProductTitlesUpdatedInHY] (ProductId, LanguageId, Title, [InsertedDate], [ProductExistsInHY])
				select
					@ProductId,
					@LanguageId,
					@Title,
					GETDATE(),
					1 -- true
					
			end
			else if @SuccessfulUpdate = 0 and @Exists = 1
			begin
	
				Update
				[integration].[tProductTitlesUpdatedInHY]
				set
					Title = @Title,
					InsertedDate = GETDATE(),
					ProductExistsInHY = 0
				where
					ProductId = @ProductId
					and LanguageId = @LanguageId
					
			end
			else if @SuccessfulUpdate = 0 and @Exists is null
			begin
			
				insert into [integration].[tProductTitlesUpdatedInHY] (ProductId, LanguageId, Title, [InsertedDate], [ProductExistsInHY])
				select
					@ProductId,
					@LanguageId,
					'',
					GETDATE(),
					0 -- false			
			end
		commit
	end try			
	begin catch
		if @@TRANCOUNT > 0 rollback
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values('ProductId: ' + @ProductId + ' LanguageId: ' + @LanguageId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

	end catch	
		
				
end
