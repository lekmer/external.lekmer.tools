﻿

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[integration].[tProductTitlesUpdatedInHY]') AND type in (N'U'))
	DROP TABLE [integration].[tProductTitlesUpdatedInHY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [integration].[tProductTitlesUpdatedInHY](
	[ProductId] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[LanguageId] [int] NOT NULL,
	[InsertedDate] [smalldatetime] NOT NULL,
	[ProductExistsInHY] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
