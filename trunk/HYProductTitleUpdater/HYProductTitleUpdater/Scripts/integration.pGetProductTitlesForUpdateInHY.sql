﻿USE [Lekmer_212]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [integration].[pGetProductTitlesForUpdateInHY] -- delete [pTranslateProductTitlesInHY]
	@ISO	nvarchar(10)
AS
begin
	set nocount on
	set transaction isolation level read uncommitted							
				
		declare @ChannelId int
		declare @LanguageId int
		
		set @LanguageId = (select LanguageId from core.tLanguage where ISO = @ISO) 
		set @ChannelId = (select ChannelId from core.tChannel where LanguageId = @LanguageId)
		
		
		if @ChannelId = 1
		begin		
			select 
				l.HYErpId, 
				t.ProductId, 
				@LanguageId as LanguageId, 
				t.Title,
				@ChannelId as ChannelId
			from 
				lekmer.tLekmerProduct l
				inner join product.tProduct t
					on l.ProductId = t.ProductId
				left join integration.tProductTitlesUpdatedInHY h
					on t.ProductId = h.ProductId
					and @LanguageId = h.LanguageId
			where 								
				t.Title != ''
				and h.ProductExistsInHY = 1
				and t.Title <> h.Title	
				or
					(
						not exists 
								(
									select 1 
									from integration.tProductTitlesUpdatedInHY i
									where i.ProductId = t.ProductId
									and i.languageId = @LanguageId					
								)
						and t.Title is not null
					)
										
		end
		else
		begin
			
			insert into product.tProductTranslation(ProductId, LanguageId)
			select
				p.ProductId,
				@LanguageId
			from
				product.tProduct p
			where
				not exists (select 1
								from product.tProductTranslation n
								where n.ProductId = p.ProductId and
								n.LanguageId = @LanguageId)
		
			select 
				l.HYErpId, 
				t.ProductId, 
				t.LanguageId as LanguageId,--@LanguageId, 
				t.Title,
				@ChannelId as ChannelId			
			from 
				lekmer.tLekmerProduct l
				inner join product.tProductTranslation t
					on l.ProductId = t.ProductId
					and t.LanguageId = @LanguageId
				left join [integration].[tProductTitlesUpdatedInHY] h
					on t.ProductId = h.ProductId
					and @LanguageId = h.LanguageId
			where 				
				(t.Title Is not null				
				and t.Title != ''
				and h.ProductExistsInHY = 1
				and t.Title <> h.Title)
				or
					(
						not exists 
								(
									select 1 
									from integration.tProductTitlesUpdatedInHY i
									where i.ProductId = t.ProductId
									and i.languageId = @LanguageId					
								)
						and t.Title is not null
					)	
				
		end
						
end
