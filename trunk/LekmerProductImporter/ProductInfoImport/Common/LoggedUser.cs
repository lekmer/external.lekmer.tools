﻿namespace ProductInfoImport
{
	public class LoggedUser
	{
		private static User _instance;

		private LoggedUser() { }

		public static User Instance
		{
			get { return _instance ?? (_instance = new User { UserName = "UNKNOWN" }); }
			set { _instance = value; }
		}
	}
}