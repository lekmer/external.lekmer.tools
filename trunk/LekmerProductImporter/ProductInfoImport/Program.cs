﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace ProductInfoImport
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			bool isAppRunning;

			using (Mutex mutex = new Mutex(true, "Lekmer Product Info Import", out isAppRunning))
			{
				if (isAppRunning)
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);

					Login loginForm = new Login();
					loginForm.ShowDialog();

					if (loginForm.DialogResult == DialogResult.OK)
					{
						Import importForm = new Import();
						Application.Run(importForm);
					}
					else
					{
						Application.Exit();
					}

					mutex.ReleaseMutex();
				}
				else
				{
					MessageBox.Show("An application instance is already running");
				}
			}
		}
	}
}