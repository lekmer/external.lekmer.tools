﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ProductInfoImport
{
	public class TagRepository
	{
		/// <summary>
		/// Gets all tags.
		/// </summary>
		public Dictionary<int, int> GetAll()
		{
			using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]))
			{
				Dictionary<int, int> tags = new Dictionary<int, int>();

				try
				{
					SqlCommand command = new SqlCommand("[lekmer].[pTagGetAllSecure]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "[lekmer].[pTagGetAllSecure]",
						CommandTimeout = 180,
					};

					connection.Open();

					IDataReader dataReader = command.ExecuteReader();
					while (dataReader.Read())
					{
						int tagId = GetTagId(dataReader);
						tags.Add(tagId, tagId);
					}
				}
				catch (Exception exception)
				{
					Import.GetInstance().LblInformation = exception.Message;
				}

				return tags;
			}
		}

		/// <summary>
		/// Get Tag Id.
		/// </summary>
		private static int GetTagId(IDataReader dataReader)
		{
			return (int)dataReader["Tag.TagId"];
		}
	}
}
