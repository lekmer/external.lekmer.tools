﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ProductInfoImport
{
	public class UserRepository
	{
		/// <summary>
		/// Gets User by user name.
		/// </summary>
		public User GetUserByUserName(string userName)
		{
			using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]))
			{
				User user = null;

				try
				{
					SqlCommand command = new SqlCommand("[security].[pSystemUserGetByUserName]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "[security].[pSystemUserGetByUserName]",
						CommandTimeout = 180,
					};

					command.Parameters.Add("@UserName", SqlDbType.NVarChar, 100).Value = userName;

					connection.Open();

					IDataReader dataReader = command.ExecuteReader();
					if (dataReader.Read())
					{
						user = GetUser(dataReader);
					}
				}
				catch (Exception exception)
				{
					Login.GetInstance().ErrorLabel = exception.Message;
				}

				return user;
			}
		}

		/// <summary>
		/// Get user from the data reader.
		/// </summary>
		private static User GetUser(IDataReader dataReader)
		{
			return new User
			{
				Id = int.Parse(dataReader["SystemUser.Id"].ToString()),
				Name = dataReader["SystemUser.Name"].ToString(),
				UserName = dataReader["SystemUser.Username"].ToString(),
				Password = dataReader["SystemUser.Password"].ToString(),
				ActivationDate = DateTime.Parse(dataReader["SystemUser.ActivationDate"].ToString()),
				ExpirationDate = DateTime.Parse(dataReader["SystemUser.ExpirationDate"].ToString()),
				UserStatus = GetUserStatus(dataReader)
			};
		}

		/// <summary>
		/// Get user status from the data reader.
		/// </summary>
		private static UserStatus GetUserStatus(IDataReader dataReader)
		{
			return new UserStatus
			{
				Id = int.Parse(dataReader["SystemUserStatus.Id"].ToString()),
				CommonName = dataReader["SystemUserStatus.CommonName"].ToString(),
				Title = dataReader["SystemUserStatus.Title"].ToString()
			};
		}
	}
}