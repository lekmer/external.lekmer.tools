﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ProductInfoImport
{
	public class ProductRepository
	{
		#region Private Members

		private static readonly string SEPERATOR = ",";

		#endregion

		#region Properties

		private string ConnectionString
		{
			get
			{
				return System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]; ;
			}
		}

		#endregion

		/// <summary>
		/// Update product info.
		/// </summary>
		/// <param name="importProductInfo">The product info to update.</param>
		/// <param name="userName">The user name.</param>
		public string UpdateProduct(ImportProductInfo importProductInfo, string userName)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				string result;

				try
				{
					SqlCommand cmd = new SqlCommand("[integration].[pBackofficeProductImportById]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "[integration].[pBackofficeProductImportById]",
						CommandTimeout = 180
					};

					string tagIds = string.Empty;
					foreach (int tagId in importProductInfo.TagIdCollection)
					{
						tagIds += tagId + SEPERATOR;
					}

					cmd.Parameters.Add("@HYErpId", SqlDbType.NVarChar).Value = importProductInfo.HYErpId;
					cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = importProductInfo.Title;
					cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = importProductInfo.Description;
					cmd.Parameters.Add("@ChannelNameISO", SqlDbType.NVarChar).Value = importProductInfo.Channel;
					cmd.Parameters.Add("@TagIds", SqlDbType.VarChar).Value = tagIds;
					cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = userName;


					connection.Open();

					cmd.ExecuteNonQuery();

					result = "Updated";
				}
				catch (Exception e)
				{
					Import.GetInstance().LblInformation = e.Message;

					result = "Failed";
				}

				return result;
			}
		}

		/// <summary>
		/// Remove old log rows from integration.tBackofficeProductInfoImport table.
		/// </summary>
		public void RemoveOldProductInfpImportLogs(int days)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				try
				{
					SqlCommand command = new SqlCommand("[integration].[pBackofficeProductInfoImportDeleteByDate]", connection)
					{
						CommandType = CommandType.StoredProcedure,
						CommandText = "[integration].[pBackofficeProductInfoImportDeleteByDate]",
						CommandTimeout = 180,
					};

					command.Parameters.Add("@Days", SqlDbType.Int).Value = days;

					connection.Open();

					command.ExecuteNonQuery();
				}
				catch (Exception exception)
				{
					Import.GetInstance().LblInformation = exception.Message;
				}
			}
		}
	}
}