﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ProductInfoImport
{
	public partial class Import : Form
	{
		private const int LOG_EXPIRATION_TIME = 7;
		private static readonly string DEFAULT_ENCODING = "ISO-8859-1";
		private static Import instance;

		private string _encodingType;
		private string EncodingType
		{
			get
			{
				if (string.IsNullOrEmpty(_encodingType))
				{
					_encodingType = System.Configuration.ConfigurationSettings.AppSettings["EncodingType"] ?? DEFAULT_ENCODING;
				}
				return _encodingType;
			}
		}

		public Import()
		{
			InitializeComponent();
			instance = this;
		}
		
		public static Import GetInstance()
		{
			return instance;
		}
		
		public string LblInformation
		{
			set { lblInformation.Text = value; }
		}
		
		public ListBox ResultList
		{
			get { return resultList; }
		}
		
		private void btnOpenFile_Click(object sender, EventArgs e)
		{
			openFileDialog.FileName = "";
			openFileDialog.ShowDialog();
			
			lblFilePath.Text = "Path: " + openFileDialog.FileName;
			ProperInformationProvided();

			btnPreview.Enabled = true;
		}
		
		public void Start()
		{
			resultList.Items.Clear();
			lblInformation.Text = "";
			ImportProductInfoHelper importProductInfoHelper = new ImportProductInfoHelper();
			
			string channel = "";
			if (cbLanguages.SelectedItem != null && !cbLanguages.SelectedItem.Equals(""))
			{
				channel = cbLanguages.SelectedItem.ToString();               
			}
			else
			{
				lblInformation.Text = "Ingen eller ogiltig fil vald.";
				return;
			}

			List<ImportProductInfo> products = importProductInfoHelper.BuildProduct(openFileDialog.FileName, channel);
			if (products != null)
			{
				btnUpdate.Enabled = false;
				btnOpenFile.Enabled = false;
				cbLanguages.Enabled = false;

				Thread updateProducts = new Thread(() => UpdateProducts(products));
				updateProducts.Start();
			}
		}
		
		private void UpdateProducts(List<ImportProductInfo> products)
		{
			ProductRepository productRepository = new ProductRepository();
			
			foreach (ImportProductInfo product in products)
			{
				string status = productRepository.UpdateProduct(product, LoggedUser.Instance.UserName);
				
				BeginInvoke(new MethodInvoker(delegate()
				{
					ResultList.Items.Add(string.Format("{0,15} {3,30} {2,30} {1,40}", product.HYErpId, status, product.Channel, DateTime.Now));
					ResultList.Refresh();
					ResultList.SelectedIndex = resultList.Items.Count - 1;
				}));
			}
			
			resultList.Enabled = true;
			
			int logExpirationTime = LOG_EXPIRATION_TIME;
			int.TryParse(System.Configuration.ConfigurationSettings.AppSettings["LogExpirationTime"], out logExpirationTime);
			productRepository.RemoveOldProductInfpImportLogs(logExpirationTime);
			
			BeginInvoke(new MethodInvoker(delegate()
			{
				lblInformation.Font = new Font(new FontFamily("Verdana"), 10, FontStyle.Bold);
				lblInformation.ForeColor = Color.DarkGreen;
				lblInformation.Text = "DONE! - " + products.Count + " produkter uppdaterade.";
				
				btnOpenFile.Enabled = true;
				cbLanguages.Enabled = true;
			}));
		}
		
		private void btnUpdate_Click(object sender, EventArgs e)
		{
			if (ProperInformationProvided())
			{               
				Start();
				Reset();
			}
		}
		
		public bool ProperInformationProvided()
		{
			if (cbLanguages.SelectedItem != null && !cbLanguages.SelectedItem.Equals(""))
			{
				if (!openFileDialog.FileName.Equals("") && !openFileDialog.FileName.Equals("openFileDialog1"))
				{
					btnUpdate.Enabled = true;
					return true;
				}
			}
			btnUpdate.Enabled = false;
			return false;
		}
		
		public void Reset()
		{
			btnUpdate.Enabled = false;
			cbLanguages.SelectedIndex = -1;            
		}
		
		private void Form1_Load(object sender, EventArgs e)
		{
			btnUpdate.Enabled = false;
		}
		
		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			ProperInformationProvided();
		}
		
		private void btnPreview_Click(object sender, EventArgs e)
		{
			if (File.Exists(openFileDialog.FileName))
			{
				Preview previewFile = new Preview();
				using (StreamReader file = new StreamReader(openFileDialog.FileName, Encoding.GetEncoding(EncodingType)))
				{
					string line;
					while ((line = file.ReadLine()) != null)
					{
						// continue if line is empty
						if (string.IsNullOrEmpty(line)) continue;
						
						previewFile.PreviewList.Items.Add(line);
					}
					
				}
				previewFile.ShowDialog();
			}
		}
	}
}