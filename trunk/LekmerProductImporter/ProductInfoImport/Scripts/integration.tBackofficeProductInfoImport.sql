/****** Object:  Table [integration].[tBackofficeProductInfoImport]    Script Date: 11/22/2011 08:22:48 ******/
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[integration].[tBackofficeProductInfoImport]') AND type in (N'U'))
	DROP TABLE [integration].[tBackofficeProductInfoImport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [integration].[tBackofficeProductInfoImport](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HYErpId] [nvarchar](20) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[ChannelNameISO] [nvarchar](10) NOT NULL,
	[TagIdCollection] [nvarchar](max) NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[InsertedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tBackofficeProductInfoImport] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO