IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[lekmer].[pTagGetAll]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [lekmer].[pTagGetAll]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [lekmer].[pTagGetAll]
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		*
	FROM 
		lekmer.[vTagSecure]
END 

GO


