/****** Object:  StoredProcedure [integration].[pBackofficeProductImportById]    Script Date: 11/22/2011 15:48:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[integration].[pBackofficeProductImportById]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [integration].[pBackofficeProductImportById]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [integration].[pBackofficeProductImportById]
	@HYErpId		nvarchar(20),
	@Title			nvarchar(256),
	@Description	nvarchar(max),
	@ChannelNameISO	nvarchar(10),
	@TagIds			VARCHAR(MAX),
	@UserName		nvarchar(100)
AS
begin
	set nocount on
	
	-- insert into tproducttransaltion
	declare @LanguageId int
	set @LanguageId = (
						select l.LanguageId 
						from core.tLanguage l
							inner join core.tChannel h
							on l.LanguageId = h.LanguageId
							inner join core.tCountry c
							on h.CountryId = c.CountryId
						where
							c.ISO = @ChannelNameISO
						)

	insert into product.tProductTranslation(ProductId, LanguageId)
	select
		p.ProductId,
		@LanguageId
	from
		product.tProduct p
	where
		not exists (select 1
						from product.tProductTranslation n
						where n.ProductId = p.ProductId and
						n.LanguageId = @LanguageId)
				
	begin try
		begin transaction				
			
		if @LanguageId != 1
			begin
				update pt
				set pt.Title = @Title
				from 
					lekmer.tLekmerProduct l
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and pt.LanguageId = @LanguageId
					and (pt.Title != @Title or pt.Title is null)
				
				
				update pt
				set pt.[Description] = @Description
				from 
					lekmer.tLekmerProduct l
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and pt.LanguageId = @LanguageId
					and (@Description is not null and @Description != '')
					and (pt.[Description] != @Description or pt.[Description] is null)
			end
		else
			begin
				update pt
				set pt.Title = @Title
				from 
					lekmer.tLekmerProduct l
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
						
				update pt
				set pt.[Description] = @Description
				from 
					lekmer.tLekmerProduct l
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					l.HYErpId = @HYErpId
					and (@Description is not null and @Description != '')	
					and (pt.[Description] != @Description or pt.[Description] is null)
			end
			
		-- Update/Insert product tags.
		DECLARE @TagId INT
		DECLARE cur_tag CURSOR FAST_FORWARD FOR
			SELECT ID FROM generic.fnConvertIDListToTable(@TagIds, ',')
			
		OPEN cur_tag
		FETCH NEXT FROM cur_tag INTO @TagId
			
		WHILE @@FETCH_STATUS = 0
			BEGIN
				-- Update/Insert product tags.
				IF NOT EXISTS (SELECT 1 FROM lekmer.tProductTag pt
						   INNER JOIN product.tProduct p ON pt.ProductId = p.ProductId	
						   INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
						   WHERE 
								lp.HYErpId = @HYErpId
								AND pt.TagId = @TagId)
					BEGIN
						INSERT INTO lekmer.tProductTag (ProductId, TagId)
						VALUES ((SELECT TOP(1) p.ProductId FROM product.tProduct p 
								INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
								WHERE lp.HYErpId = @HYErpId), @TagId)
					END
				
				FETCH NEXT FROM cur_tag INTO @TagId
			END
		CLOSE cur_tag
		DEALLOCATE cur_tag
		
		-- Insert data about inserted product information and user into integration.tBackofficeProductInfoImport table.
		insert into integration.tBackofficeProductInfoImport
		(
			[HYErpId]
			,[Title]
			,[Description]
			,[ChannelNameISO]
			,[TagIdCollection]
			,[UserName]
			,[InsertedDate]
		)
		VALUES 
		(
			@HYErpId
			,@Title
			,@Description
			,@ChannelNameISO
			,@TagIds
			,@UserName
			,CAST(GETDATE() AS smalldatetime)
		)
		
		commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO


