/****** Object:  StoredProcedure [integration].[pBackofficeProductInfoImportDeleteByDate]    Script Date: 11/22/2011 11:16:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[integration].[pBackofficeProductInfoImportDeleteByDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [integration].[pBackofficeProductInfoImportDeleteByDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductInfoImportDeleteByDate]
	@Days int
AS
begin
	set nocount on
	
	-- Current date.
	declare @CurrentDate as datetime
	set @CurrentDate = GETDATE()
	
	-- Deletion date.
	declare @DeletionDate as datetime
	set @DeletionDate = DATEADD(day, -@Days, @CurrentDate)
	
	-- Delete old rows.
	delete from integration.tBackofficeProductInfoImport
	where [InsertedDate] < @DeletionDate	 
end
GO


