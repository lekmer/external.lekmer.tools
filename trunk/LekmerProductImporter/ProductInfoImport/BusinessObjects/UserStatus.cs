﻿namespace ProductInfoImport
{
	public class UserStatus
	{
		public int Id { get; set; }
		public string CommonName { get; set; }
		public string Title { get; set; }
	}
}