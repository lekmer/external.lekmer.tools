﻿using System;

namespace ProductInfoImport
{
	public class User
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public DateTime? ActivationDate { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public UserStatus UserStatus { get; set; }
	}
}