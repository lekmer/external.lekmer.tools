﻿using System.Collections.Generic;

namespace ProductInfoImport
{
	public class ImportProductInfo
	{
		public string HYErpId { get; set; }
		public string Channel { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public List<int> TagIdCollection { get; set; }
	}
}