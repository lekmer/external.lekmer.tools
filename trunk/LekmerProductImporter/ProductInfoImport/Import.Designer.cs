﻿namespace ProductInfoImport
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.btnOpenFile = new System.Windows.Forms.Button();
			this.lblInformation = new System.Windows.Forms.Label();
			this.resultList = new System.Windows.Forms.ListBox();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.lblFilePath = new System.Windows.Forms.Label();
			this.cbLanguages = new System.Windows.Forms.ComboBox();
			this.lblChannel = new System.Windows.Forms.Label();
			this.btnPreview = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// openFileDialog
			// 
			this.openFileDialog.FileName = "openFileDialog1";
			// 
			// btnOpenFile
			// 
			this.btnOpenFile.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnOpenFile.Location = new System.Drawing.Point(12, 12);
			this.btnOpenFile.Name = "btnOpenFile";
			this.btnOpenFile.Size = new System.Drawing.Size(105, 31);
			this.btnOpenFile.TabIndex = 0;
			this.btnOpenFile.Text = "Open file";
			this.btnOpenFile.UseVisualStyleBackColor = true;
			this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
			// 
			// lblInformation
			// 
			this.lblInformation.Font = new System.Drawing.Font("Verdana", 8.25F);
			this.lblInformation.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblInformation.Location = new System.Drawing.Point(12, 283);
			this.lblInformation.Name = "lblInformation";
			this.lblInformation.Size = new System.Drawing.Size(429, 71);
			this.lblInformation.TabIndex = 1;
			// 
			// resultList
			// 
			this.resultList.FormattingEnabled = true;
			this.resultList.Location = new System.Drawing.Point(14, 125);
			this.resultList.Name = "resultList";
			this.resultList.Size = new System.Drawing.Size(539, 186);
			this.resultList.TabIndex = 2;
			// 
			// btnUpdate
			// 
			this.btnUpdate.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnUpdate.Location = new System.Drawing.Point(447, 318);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.Size = new System.Drawing.Size(106, 34);
			this.btnUpdate.TabIndex = 3;
			this.btnUpdate.Text = "Update";
			this.btnUpdate.UseVisualStyleBackColor = true;
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			// 
			// lblFilePath
			// 
			this.lblFilePath.AutoSize = true;
			this.lblFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblFilePath.Location = new System.Drawing.Point(127, 21);
			this.lblFilePath.Name = "lblFilePath";
			this.lblFilePath.Size = new System.Drawing.Size(0, 15);
			this.lblFilePath.TabIndex = 4;
			// 
			// cbLanguages
			// 
			this.cbLanguages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLanguages.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.cbLanguages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.cbLanguages.FormattingEnabled = true;
			this.cbLanguages.Items.AddRange(new object[] {
            "SE",
            "NO",
            "DK",
            "FI",
            "NL"});
			this.cbLanguages.Location = new System.Drawing.Point(130, 95);
			this.cbLanguages.Name = "cbLanguages";
			this.cbLanguages.Size = new System.Drawing.Size(121, 23);
			this.cbLanguages.TabIndex = 5;
			this.cbLanguages.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// lblChannel
			// 
			this.lblChannel.AutoSize = true;
			this.lblChannel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lblChannel.Location = new System.Drawing.Point(12, 98);
			this.lblChannel.Name = "lblChannel";
			this.lblChannel.Size = new System.Drawing.Size(105, 14);
			this.lblChannel.TabIndex = 6;
			this.lblChannel.Text = "Select Channel";
			// 
			// btnPreview
			// 
			this.btnPreview.Enabled = false;
			this.btnPreview.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
			this.btnPreview.Location = new System.Drawing.Point(12, 49);
			this.btnPreview.Name = "btnPreview";
			this.btnPreview.Size = new System.Drawing.Size(105, 31);
			this.btnPreview.TabIndex = 7;
			this.btnPreview.Text = "Preview file";
			this.btnPreview.UseVisualStyleBackColor = true;
			this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
			// 
			// Import
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 398);
			this.Controls.Add(this.btnPreview);
			this.Controls.Add(this.lblChannel);
			this.Controls.Add(this.cbLanguages);
			this.Controls.Add(this.lblFilePath);
			this.Controls.Add(this.btnUpdate);
			this.Controls.Add(this.resultList);
			this.Controls.Add(this.lblInformation);
			this.Controls.Add(this.btnOpenFile);
			this.Name = "Import";
			this.Text = "Lekmer updater v0.2 beta";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label lblInformation;
        private System.Windows.Forms.ListBox resultList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.ComboBox cbLanguages;
		private System.Windows.Forms.Label lblChannel;
		private System.Windows.Forms.Button btnPreview;
    }
}

