﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ProductInfoImport
{
	public class ImportProductInfoHelper
	{
		#region Private Members

		private static readonly string DEFAULT_ENCODING = "ISO-8859-1";
		private static readonly int MIN_COLUMN_COUNT = 3;
		private string _rowErrorMessage;
		private string _tagRowErrorMessage;

		#endregion

		#region Properties

		private string RowErrorMessage
		{
			get
			{
				if (string.IsNullOrEmpty(_rowErrorMessage))
				{
					_rowErrorMessage = System.Configuration.ConfigurationSettings.AppSettings["RowErrorMessage"];
				}
				return _rowErrorMessage;
			}
		}

		private string TagRowErrorMessage
		{
			get
			{
				if (string.IsNullOrEmpty(_tagRowErrorMessage))
				{
					_tagRowErrorMessage = System.Configuration.ConfigurationSettings.AppSettings["TagRowErrorMessage"];
				}
				return _tagRowErrorMessage;
			}
		}

		#endregion

		public List<ImportProductInfo> BuildProduct(string filePath, string channel)
		{
			if (File.Exists(filePath))
			{
				StreamReader file = null;
				try
				{
					TagRepository tagRepository = new TagRepository();
					Dictionary<int, int> tagIds = tagRepository.GetAll();

					bool isFileValid = true;

					List<ImportProductInfo> products = new List<ImportProductInfo>();
					//char[] delimiterChars = { '\t', ';' };
					char[] delimiterChars = { '\t' };

					string encodingType = System.Configuration.ConfigurationSettings.AppSettings["EncodingType"] ?? DEFAULT_ENCODING; //note windows something
					file = new StreamReader(filePath, Encoding.GetEncoding(encodingType));

					string line;
					while ((line = file.ReadLine()) != null)
					{
						// continue if line is empty.
						if (string.IsNullOrEmpty(line)) continue;

						string[] info = line.Split(delimiterChars);

						if (info.Length < MIN_COLUMN_COUNT)
						{
							BuildErrorMessage(line, RowErrorMessage);
							isFileValid = false;
							continue;
						}

						ImportProductInfo tmpProduct = new ImportProductInfo
						{
							HYErpId = info[0],
							Title = info[1],
							Description = info[2],
							Channel = channel,
							TagIdCollection = new List<int>()
						};

						if (info.Length > MIN_COLUMN_COUNT)
						{
							for (int i = MIN_COLUMN_COUNT; i < info.Length; i++)
							{
								int tagId;
								int.TryParse(info[i], out tagId);
								if (tagId <= 0) continue;

								if (tagIds.ContainsKey(tagId))
								{
									tmpProduct.TagIdCollection.Add(tagId);
								}
								else
								{
									BuildErrorMessage(line, TagRowErrorMessage);
									isFileValid = false;
									break;
								}
							}
						}

						products.Add(tmpProduct);
					}

					if (isFileValid)
					{
						return products;
					}
					else
					{
						Import.GetInstance().ResultList.Refresh();
						return null;
					}
				}
				catch (Exception e)
				{
					if (file != null)
					{
						file.Close();
					}
					Import.GetInstance().LblInformation = e.Message;
				}
				finally
				{
					if (file != null)
					{
						file.Close();
					}
				}
			}

			return null;
		}

		private void BuildErrorMessage(string row, string errorMessage)
		{
			Import.GetInstance().ResultList.Items.Add(string.Format(errorMessage, row));
		}
	}
}