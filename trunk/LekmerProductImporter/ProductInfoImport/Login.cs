﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace ProductInfoImport
{
	public partial class Login : Form
	{
		private static Login instance;
		public static Login GetInstance()
        {
            return instance;
        }

		public string ErrorLabel
		{
			set { lblError.Text = value; }
		}

		public Login()
		{
			InitializeComponent();
			instance = this;
			txtPassword.PasswordChar = '*';
		}

		/// <summary>
		/// Handle Sign In button click event.
		/// </summary>
		private void SignIn(object sender, EventArgs e)
		{
			string userName = txtUserName.Text; //"admin@litium.se";
			string password = txtPassword.Text; //"spongebob";
			
			// Check if parmeters are correct.
			if (!ValidateInputParameters(userName, password)) return;

			// Get User from database by user name.
			UserRepository userRepository = new UserRepository();
			User user = userRepository.GetUserByUserName(userName);

			// Validate user properties and password
			if (!ValidateUser(password, user)) return;

			LoggedUser.Instance = user;

			DialogResult = DialogResult.OK;
		}

		/// <summary>
		/// Validate if user name and password have values.
		/// </summary>
		/// <param name="userName">The user name value.</param>
		/// <param name="password">The password value.</param>
		/// <returns>Returns true if user name and password have a values, else returns false.</returns>
		private bool ValidateInputParameters(string userName, string password)
		{
			bool isValidParameters = true;
			StringBuilder errors = new StringBuilder();

			if (string.IsNullOrEmpty(userName))
			{
				errors.AppendLine("Please enter user name.");
			}

			if (string.IsNullOrEmpty(password))
			{
				errors.AppendLine("Please enter password.");
			}

			if (errors.Length > 0)
			{
				isValidParameters = false;

				lblError.Text = errors.ToString();
				lblError.Visible = true;
			}

			return isValidParameters;
		}

		/// <summary>
		/// Validate user properties.
		/// </summary>
		/// <param name="password">The password value.</param>
		/// <param name="user">The user object.</param>
		/// <returns>Returns true if user has valid properties else returns false.</returns>
		private bool ValidateUser(string password, User user)
		{
			bool isUserValid = true;
			StringBuilder errors = new StringBuilder();

			if (user == null)
			{
				errors.AppendLine("Invalid usename.");
			}
			else if (user.UserStatus.CommonName == "Offline")
			{
				errors.AppendLine("User status is offline that may be a result of exceeding max count of invalid logins.");
			}
			else if (user.ExpirationDate.HasValue && user.ExpirationDate.Value < DateTime.Now)
			{
				errors.AppendLine("Password has expired.");
			}
			else if (user.ActivationDate.HasValue && user.ActivationDate.Value > DateTime.Now)
			{
				errors.AppendLine("User activation date is not reached yet.");
			}
			else if (!ValidatePassword(user.Password, password))
			{
				errors.AppendLine("Invalid password.");
			}

			if (errors.Length > 0)
			{
				isUserValid = false;

				lblError.Text = errors.ToString();
				lblError.Visible = true;
			}

			return isUserValid;
		}

		/// <summary>
		/// Validate the password.
		/// </summary>
		/// <param name="userPassword">The encrypted password.</param>
		/// <param name="password">The password from login form.</param>
		private bool ValidatePassword(string userPassword, string password)
		{
			return CreateEncryptedPassword(password).Equals(userPassword);
		}

		/// <summary>
		/// Create encrypted password.
		/// </summary>
		/// <param name="password">>The password from login form.</param>
		public virtual string CreateEncryptedPassword(string password)
		{
			byte[] hash;
			using (SHA1CryptoServiceProvider cryptoServiceProvider = new SHA1CryptoServiceProvider())
			{
				hash = cryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(password));
			}

			return BitConverter.ToString(hash).Replace("-", string.Empty);
		}
	}
}
