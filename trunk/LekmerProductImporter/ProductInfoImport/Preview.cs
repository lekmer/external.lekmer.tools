﻿using System.Windows.Forms;

namespace ProductInfoImport
{
	public partial class Preview : Form
	{
		private static Preview instance;

		public ListBox PreviewList
		{
			get { return lbPreview; }
		}

		public static Preview GetInstance()
		{
			return instance;
		}

		public Preview()
		{
			InitializeComponent();
		}
	}
}