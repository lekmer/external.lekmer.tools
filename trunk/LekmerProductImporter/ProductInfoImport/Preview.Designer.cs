﻿namespace ProductInfoImport
{
	partial class Preview
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbPreview = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// lbPreview
			// 
			this.lbPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbPreview.FormattingEnabled = true;
			this.lbPreview.HorizontalScrollbar = true;
			this.lbPreview.Location = new System.Drawing.Point(1, 1);
			this.lbPreview.Name = "lbPreview";
			this.lbPreview.Size = new System.Drawing.Size(629, 264);
			this.lbPreview.TabIndex = 0;
			// 
			// PreviewFile
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(630, 262);
			this.Controls.Add(this.lbPreview);
			this.Name = "PreviewFile";
			this.Text = "PreviewFile";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lbPreview;
	}
}