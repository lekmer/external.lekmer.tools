﻿namespace Lekmer.UICompressor
{
	public enum CompressionType
	{
		/// <summary>
		/// None => Concatenate files only.
		/// </summary>
		Unknown = 0,
		
		/// <summary>
		/// None => Concatenate files only.
		/// </summary>
		None = 10,

		/// <summary>
		/// 
		/// </summary>
		StandardYui = 20
	}
}
