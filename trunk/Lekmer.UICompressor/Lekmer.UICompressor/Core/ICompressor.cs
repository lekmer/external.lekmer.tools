﻿namespace Lekmer.UICompressor
{
	public interface ICompressor
	{
		CompressionType CompressionType { get; set; }

		string Compress(string source);
	}
}
