﻿using System;

namespace Lekmer.UICompressor
{
	public interface IFilesGroupWatcher : IDisposable
	{
		event EventHandler Changed;
		void StartWatching(bool filesAreChanged = false);
	}
}
