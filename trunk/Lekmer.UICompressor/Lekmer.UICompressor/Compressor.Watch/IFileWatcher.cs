﻿using System;
using System.IO;

namespace Lekmer.UICompressor
{
	public interface IFileWatcher : IDisposable
	{
		IFileSpec FileSpec { get; }
		event FileSystemEventHandler Changed;
		void StartWatching();
	}
}
