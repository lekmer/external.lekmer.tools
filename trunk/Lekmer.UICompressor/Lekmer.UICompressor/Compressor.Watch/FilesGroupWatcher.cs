﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Timers;
using log4net;

namespace Lekmer.UICompressor
{
	public class FilesGroupWatcher : IFilesGroupWatcher
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly object _syncToken = new object();
		private readonly List<IFileWatcher> _fileWatchers;
		private readonly Timer _timer;
		private bool _filesChanged;


		public FilesGroupWatcher(List<IFileSpec> fileSpecs)
		{
			_fileWatchers = new List<IFileWatcher>(fileSpecs.Count);

			foreach (IFileSpec fileSpec in fileSpecs)
			{
				var fileWatcher = new FileWatcher(fileSpec);
				fileWatcher.Changed += OnFileChanged;

				_fileWatchers.Add(fileWatcher);
			}

			_timer = new Timer();

			_timer.Elapsed += OnTimerElapsed;
			_timer.Interval = 1000 * 60; // 60 sec
			_timer.AutoReset = false;
		}


		public event EventHandler Changed;

		public virtual void StartWatching(bool filesAreChanged = false)
		{
			foreach (IFileWatcher fileWatcher in _fileWatchers)
			{
				fileWatcher.StartWatching();
			}

			if (filesAreChanged)
			{
				_filesChanged = true;
				_timer.Interval = 1000 * 10; // 10 sec
			}

			_timer.Start(); // Sets Enabled=true
		}


		protected virtual void OnFileChanged(object sender, FileSystemEventArgs e)
		{
			if (_filesChanged == false)
			{
				_log.InfoFormat("Change detected  to: {0}", Path.GetFileName(((IFileWatcher)sender).FileSpec.FileName));

				lock (_syncToken)
				{
					_filesChanged = true;
					_timer.Interval = 1000 * 10; // 10 sec
				}
			}
		}

		protected virtual void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			bool pendingChange = false;

			if(_filesChanged)
			{
				lock (_syncToken)
				{
					_filesChanged = false;
					pendingChange = true;
					_timer.Interval = 1000 * 60 ; // 60 sec
				}
			}

			if (pendingChange)
			{
				OnChanged(EventArgs.Empty);
				_timer.Start();
			}
		}

		protected virtual void OnChanged(EventArgs e)
		{
			EventHandler handler = Changed;
			if (handler != null)
			{
				handler(this, e);
			}
		}


		// IDisposable

		public virtual void Dispose()
		{
			if (_timer != null)
			{
				_timer.Dispose();
			}

			if (_fileWatchers != null)
			{
				foreach (IFileWatcher fileWatcher in _fileWatchers)
				{
					if (fileWatcher != null)
					{
						fileWatcher.Dispose();
					}
				}
			}
		}
	}
}
