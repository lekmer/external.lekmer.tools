﻿using System.IO;

namespace Lekmer.UICompressor
{
	public class FileWatcher : IFileWatcher
	{
		private FileSystemWatcher _watcher;

		public FileWatcher(IFileSpec fileSpec)
		{
			FileSpec = fileSpec;
		}

		public IFileSpec FileSpec { get; private set; }
		public event FileSystemEventHandler Changed;

		public void StartWatching()
		{
			_watcher = new FileSystemWatcher();

			_watcher.Path = Path.GetDirectoryName(FileSpec.FileName);
			_watcher.Filter = Path.GetFileName(FileSpec.FileName);

			_watcher.NotifyFilter =
				NotifyFilters.Attributes |
				NotifyFilters.CreationTime |
				NotifyFilters.LastAccess |
				NotifyFilters.LastWrite |
				NotifyFilters.Size;

			_watcher.Changed += OnChanged;
			_watcher.Created += OnChanged;
			_watcher.Deleted += OnChanged;

			_watcher.EnableRaisingEvents = true;
		}

		protected virtual void OnChanged(object sender, FileSystemEventArgs e)
		{
			FileSystemEventHandler handler = Changed;
			if (handler != null)
			{
				handler(this, e);
			}
		}


		// IDisposable

		public virtual void Dispose()
		{
			if (_watcher != null)
			{
				_watcher.Dispose();
			}
		}
	}
}
