﻿namespace Lekmer.UICompressor.Utilities
{
	/// <summary>
	/// Some usefull System.String extensions.
	/// </summary>
	public static class StringExtensions
	{
		/// <summary>
		/// Indicates whether the specified System.String object is null or an System.String.Empty string.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool IsEmpty(this string value)
		{
			return value == null || value.Length == 0;
		}

		/// <summary>
		/// Indicates whether the specified System.String object is not null and not System.String.Empty string.
		/// </summary>
		/// <param name="value">A String reference.</param>
		public static bool HasValue(this string value)
		{
			return value != null && value.Length > 0;
		}
	}
}
