﻿using System.Globalization;
using System.Text;

namespace Lekmer.UICompressor
{
	public interface IJsCompressorOptionsYui
	{
		/// <summary>
		/// The position where a line feed is appened when the next semicolon is reached.
		/// Default is -1 (never add a line break).
		/// 0 (zero) means add a line break after every semicolon. (This might help with debugging troublesome files).
		/// </summary>
		int LineBreakPosition { get; set; }

		/// <summary>
		/// EncodingType: ASCII, BigEndianUnicode, Unicode, UTF32, UTF7, UTF8, Default (default).
		/// </summary>
		Encoding Encoding { get; set; }

		/// <summary>
		/// True | False (default).
		/// </summary>
		bool DisableOptimizations { get; set; }

		/// <summary>
		/// True => compress any functions that contain 'eval'. Default is False, which means a function that contains
		/// 'eval' will NOT be compressed. It's deemed risky to compress a function containing 'eval'. That said,
		/// if the usages are deemed safe this check can be disabled by setting this value to True.
		/// </summary>
		bool IgnoreEval { get; set; }

		/// <summary>
		/// True => Obfuscate function and variable names.
		/// True (default).
		/// </summary>
		bool ObfuscateJavascript { get; set; }

		/// <summary>
		/// True => preserve redundant semicolons (e.g. after a '}'.
		/// </summary>
		bool PreserveAllSemicolons { get; set; }

		/// <summary>
		/// The culture you want the thread to run under. This affects the treatment of numbers etc - e.g. 9.00 could be output as 9,00.
		/// Default value is the Invariant Culture.
		/// </summary>
		CultureInfo ThreadCulture { get; set; }
	}
}
