﻿namespace Lekmer.UICompressor
{
	public interface ICssCompressorOptionsYui
	{
		/// <summary>
		/// The position where a line feed is appened when the next semicolon is reached.
		/// Default is -1 (never add a line break).
		/// 0 (zero) means add a line break after every semicolon. (This might help with debugging troublesome files).
		/// </summary>
		int LineBreakPosition { get; set; }

		/// <summary>
		/// Set True if you wish to preserve css comments.  False will remove them except ones marked with "!" 
		/// </summary>
		bool PreserveComments { get; set; }
	}
}
