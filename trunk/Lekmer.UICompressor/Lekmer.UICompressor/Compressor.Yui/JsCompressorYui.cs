﻿using System;
using Yahoo.Yui.Compressor;

namespace Lekmer.UICompressor
{
	public class JsCompressorYui : ICompressor
	{
		private IJavaScriptCompressor _compressor;


		protected IJsCompressorOptionsYui Options { get; set; }

		protected IJavaScriptCompressor Compressor
		{
			get { return _compressor ?? (_compressor = CreateCompressor()); }
		}


		public JsCompressorYui(IJsCompressorOptionsYui options)
		{
			Options = options;
		}


		public CompressionType CompressionType { get; set; }

		public virtual string Compress(string source)
		{
			SetCompressorType();

			return Compressor.Compress(source);
		}


		protected virtual IJavaScriptCompressor CreateCompressor()
		{
			var compressor = new JavaScriptCompressor();

			SetCompressorParameters(compressor);

			return compressor;
		}

		protected virtual void SetCompressorParameters(IJavaScriptCompressor compressor)
		{
			compressor.LineBreakPosition = Options.LineBreakPosition;
			compressor.Encoding = Options.Encoding;
			compressor.DisableOptimizations = Options.DisableOptimizations;
			compressor.IgnoreEval = Options.IgnoreEval;
			compressor.ObfuscateJavascript = Options.ObfuscateJavascript;
			compressor.PreserveAllSemicolons = Options.PreserveAllSemicolons;
			compressor.ThreadCulture = Options.ThreadCulture;
		}

		protected virtual void SetCompressorType()
		{
			switch (CompressionType)
			{
				case CompressionType.None:
					Compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.None;
					break;
				case CompressionType.StandardYui:
					Compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.Standard;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
