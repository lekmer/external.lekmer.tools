﻿namespace Lekmer.UICompressor
{
	public class CssCompressorOptionsYui : ICssCompressorOptionsYui
	{
		public int LineBreakPosition { get; set; }
		public bool PreserveComments { get; set; }
	}
}
