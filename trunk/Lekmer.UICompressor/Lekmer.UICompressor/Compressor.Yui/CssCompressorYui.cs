﻿using System;
using Yahoo.Yui.Compressor;

namespace Lekmer.UICompressor
{
	public class CssCompressorYui : ICompressor
	{
		private ICssCompressor _compressor;


		protected ICssCompressorOptionsYui Options { get; set; }

		protected ICssCompressor Compressor
		{
			get { return _compressor ?? (_compressor = CreateCompressor()); }
		}


		public CssCompressorYui(ICssCompressorOptionsYui options)
		{
			Options = options;
		}


		public CompressionType CompressionType { get; set; }

		public virtual string Compress(string source)
		{
			SetCompressorType();

			return Compressor.Compress(source);
		}


		protected virtual ICssCompressor CreateCompressor()
		{
			var compressor = new CssCompressor();

			SetCompressorParameters(compressor);

			return compressor;
		}

		protected virtual void SetCompressorParameters(ICssCompressor compressor)
		{
			compressor.LineBreakPosition = Options.LineBreakPosition;
			compressor.RemoveComments = !Options.PreserveComments;
		}

		protected virtual void SetCompressorType()
		{
			switch (CompressionType)
			{
				case CompressionType.None:
					Compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.None;
					break;
				case CompressionType.StandardYui:
					Compressor.CompressionType = Yahoo.Yui.Compressor.CompressionType.Standard;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
