﻿using System.Globalization;
using System.Text;

namespace Lekmer.UICompressor
{
	public class JsCompressorOptionsYui : IJsCompressorOptionsYui
	{
		public int LineBreakPosition { get; set; }
		public Encoding Encoding { get; set; }
		public bool DisableOptimizations { get; set; }
		public bool IgnoreEval { get; set; }
		public bool ObfuscateJavascript { get; set; }
		public bool PreserveAllSemicolons { get; set; }
		public CultureInfo ThreadCulture { get; set; }
	}
}
