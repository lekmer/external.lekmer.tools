﻿using System.Globalization;
using System.Text;

namespace Lekmer.UICompressor
{
	public interface IJsCompressorJob
	{
		string OutputFile { get; set; }
		CompressionType CompressionType { get; set; }
		Encoding Encoding { get; set; }
		int LineBreakPosition { get; set; }
		bool ObfuscateJavaScript { get; set; }
		bool PreserveAllSemicolons { get; set; }
		bool DisableOptimizations { get; set; }
		CultureInfo ThreadCulture { get; set; }
		bool IsEvalIgnored { get; set; }

		string Compress(IFileSpec fileSpec, string originalContent);
	}
}
