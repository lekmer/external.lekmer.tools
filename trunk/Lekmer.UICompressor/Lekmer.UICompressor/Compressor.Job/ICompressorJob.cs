﻿using System;
using System.Collections.Generic;

namespace Lekmer.UICompressor
{
	public interface ICompressorJob : IDisposable
	{
		string Name { get; set; }

		List<IFileSpec> CssFiles { get; set; }
		List<IFileSpec> JsFiles { get; set; }

		List<ICssCompressorJob> CssCompressorJobs { get; set; }
		List<IJsCompressorJob> JsCompressorJobs { get; set; }

		void Execute();
	}
}
