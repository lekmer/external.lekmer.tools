﻿using System.Collections.Generic;

namespace Lekmer.UICompressor
{
	public class CompressorJobList : ICompressorJobList
	{
		public List<ICompressorJob> CompressorJobs { get; set; }

		public virtual void Execute()
		{
			foreach (ICompressorJob compressorJob in CompressorJobs)
			{
				compressorJob.Execute();
			}
		}

		// IDisposable

		public virtual void Dispose()
		{
			if (CompressorJobs != null)
			{
				foreach (ICompressorJob compressorJob in CompressorJobs)
				{
					compressorJob.Dispose();
				}
			}
		}
	}
}
