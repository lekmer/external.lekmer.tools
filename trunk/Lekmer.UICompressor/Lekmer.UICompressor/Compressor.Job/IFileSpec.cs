﻿namespace Lekmer.UICompressor
{
	public interface IFileSpec
	{
		string FileName { get; }
		CompressionType CompressionType { get; }
	}
}
