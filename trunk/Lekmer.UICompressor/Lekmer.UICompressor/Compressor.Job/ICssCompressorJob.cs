﻿using System.Text;

namespace Lekmer.UICompressor
{
	public interface ICssCompressorJob
	{
		string OutputFile { get; set; }
		CompressionType CompressionType { get; set; }
		Encoding Encoding { get; set; }
		int LineBreakPosition { get; set; }
		bool PreserveComments { get; set; }

		string Compress(IFileSpec fileSpec, string originalContent);
	}
}
