﻿namespace Lekmer.UICompressor
{
	public class FileSpec : IFileSpec
	{
		public string FileName { get; private set; }
		public CompressionType CompressionType { get; private set; }

		public FileSpec(string fileName, CompressionType compressionType)
		{
			FileName = fileName;
			CompressionType = compressionType;
		}
	}
}
