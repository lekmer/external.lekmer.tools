﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Lekmer.UICompressor.Utilities;
using log4net;

namespace Lekmer.UICompressor
{
	public class CompressorJob : ICompressorJob
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IFilesGroupWatcher CssFilesWatcher { get; set; }
		protected IFilesGroupWatcher JsFilesWatcher { get; set; }

		public string Name { get; set; }

		public List<IFileSpec> CssFiles { get; set; }
		public List<IFileSpec> JsFiles { get; set; }

		public List<ICssCompressorJob> CssCompressorJobs { get; set; }
		public List<IJsCompressorJob> JsCompressorJobs { get; set; }


		public virtual void Execute()
		{
			_log.InfoFormat("[{0}] Starting compression...", Name);

			try
			{
				foreach (ICssCompressorJob cssCompressorJob in CssCompressorJobs)
				{
					CompressFiles(cssCompressorJob);
				}

				foreach (IJsCompressorJob jsCompressorJob in JsCompressorJobs)
				{
					CompressFiles(jsCompressorJob);
				}
			}
			catch (Exception ex)
			{
				_log.Error("Compression failure.", ex);
			}

			_log.InfoFormat("[{0}] Finished compression.", Name);

			StartWatching();
		}


		protected virtual void CompressFiles(ICssCompressorJob compressorJob)
		{
			int totalOriginalContentLength = 0;
			var finalContent = new StringBuilder();

			foreach (IFileSpec file in CssFiles)
			{
				string originalContent = File.ReadAllText(file.FileName, compressorJob.Encoding);
				totalOriginalContentLength += originalContent.Length;

				string compressedContent = compressorJob.Compress(file, originalContent);

				if (compressedContent.HasValue())
				{
					finalContent.Append(compressedContent);
				}
			}

			File.WriteAllText(compressorJob.OutputFile, finalContent.ToString(), compressorJob.Encoding);
		}

		protected virtual void CompressFiles(IJsCompressorJob compressorJob)
		{
			int totalOriginalContentLength = 0;
			var finalContent = new StringBuilder();

			foreach (IFileSpec file in JsFiles)
			{
				string originalContent = File.ReadAllText(file.FileName, compressorJob.Encoding);
				totalOriginalContentLength += originalContent.Length;

				string compressedContent = compressorJob.Compress(file, originalContent);

				if (compressedContent.HasValue())
				{
					finalContent.Append(compressedContent);
				}
			}

			File.WriteAllText(compressorJob.OutputFile, finalContent.ToString(), compressorJob.Encoding);
		}

		protected virtual void StartWatching()
		{
			if (CssFilesWatcher == null)
			{
				CssFilesWatcher = new FilesGroupWatcher(CssFiles);
				CssFilesWatcher.Changed += CssFilesWatcherOnChanged;
				CssFilesWatcher.StartWatching();
			}

			if (JsFilesWatcher == null)
			{
				JsFilesWatcher = new FilesGroupWatcher(JsFiles);
				JsFilesWatcher.Changed += JsFilesWatcherOnChanged;
				JsFilesWatcher.StartWatching();
			}
		}

		protected virtual void CssFilesWatcherOnChanged(object sender, EventArgs eventArgs)
		{
			_log.InfoFormat("[{0}] Starting css compression...", Name);

			try
			{
				foreach (ICssCompressorJob cssCompressorJob in CssCompressorJobs)
				{
					CompressFiles(cssCompressorJob);
				}
			}
			catch (Exception ex)
			{
				_log.Error("Compression css failure.", ex);
			}

			_log.InfoFormat("[{0}] Finished css compression.", Name);
		}

		protected virtual void JsFilesWatcherOnChanged(object sender, EventArgs eventArgs)
		{
			_log.InfoFormat("[{0}] Starting js compression...", Name);

			try
			{
				foreach (IJsCompressorJob jsCompressorJob in JsCompressorJobs)
				{
					CompressFiles(jsCompressorJob);
				}
			}
			catch (Exception ex)
			{
				_log.Error("Compression js failure.", ex);
			}

			_log.InfoFormat("[{0}] Finished js compression.", Name);
		}


		// IDisposable

		public virtual void Dispose()
		{
			if (CssFilesWatcher != null)
			{
				CssFilesWatcher.Dispose();
			}

			if (JsFilesWatcher != null)
			{
				JsFilesWatcher.Dispose();
			}
		}
	}
}
