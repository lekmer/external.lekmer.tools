﻿using System;
using System.Collections.Generic;

namespace Lekmer.UICompressor
{
	public interface ICompressorJobList : IDisposable
	{
		List<ICompressorJob> CompressorJobs { get; set; }

		void Execute();
	}
}
