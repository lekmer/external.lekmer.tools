﻿using System.Text;

namespace Lekmer.UICompressor
{
	public class CssCompressorJob : ICssCompressorJob
	{
		private ICompressor _compressor;

		public string OutputFile { get; set; }
		public CompressionType CompressionType { get; set; }
		public Encoding Encoding { get; set; }
		public int LineBreakPosition { get; set; }
		public bool PreserveComments { get; set; }

		protected ICompressor Compressor
		{
			get { return _compressor ?? (_compressor = CreateCompressor()); }
		}


		public virtual string Compress(IFileSpec fileSpec, string originalContent)
		{
			Compressor.CompressionType = GetCompressionTypeFor(fileSpec);

			return Compressor.Compress(originalContent);
		}

		protected virtual ICompressor CreateCompressor()
		{
			return new CssCompressorYui(CreateCompressorOptions());
		}

		protected virtual ICssCompressorOptionsYui CreateCompressorOptions()
		{
			return new CssCompressorOptionsYui
			{
				LineBreakPosition = LineBreakPosition,
				PreserveComments = PreserveComments
			};
		}

		protected virtual CompressionType GetCompressionTypeFor(IFileSpec fileSpec)
		{
			return fileSpec.CompressionType > CompressionType.Unknown
				? fileSpec.CompressionType 
				: CompressionType;
		}
	}
}
