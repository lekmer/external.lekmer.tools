﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lekmer.UICompressor
{
	public interface ISettingXmlParser
	{
		ICompressorJobList Parse(string settingXmlSource);
	}
}
