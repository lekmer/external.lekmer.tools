﻿using System.Globalization;
using System.Text;

namespace Lekmer.UICompressor
{
	public class JsCompressorJob : IJsCompressorJob
	{
		private ICompressor _compressor;

		public string OutputFile { get; set; }
		public CompressionType CompressionType { get; set; }
		public Encoding Encoding { get; set; }
		public int LineBreakPosition { get; set; }
		public bool ObfuscateJavaScript { get; set; }
		public bool PreserveAllSemicolons { get; set; }
		public bool DisableOptimizations { get; set; }
		public CultureInfo ThreadCulture { get; set; }
		public bool IsEvalIgnored { get; set; }

		protected ICompressor Compressor
		{
			get { return _compressor ?? (_compressor = CreateCompressor()); }
		}

		public virtual string Compress(IFileSpec fileSpec, string originalContent)
		{
			Compressor.CompressionType = GetCompressionTypeFor(fileSpec);

			return Compressor.Compress(originalContent);
		}

		protected virtual ICompressor CreateCompressor()
		{
			return new JsCompressorYui(CreateCompressorOptions());
		}

		protected virtual IJsCompressorOptionsYui CreateCompressorOptions()
		{
			return new JsCompressorOptionsYui
			{
				LineBreakPosition = LineBreakPosition,
				Encoding = Encoding,
				DisableOptimizations = DisableOptimizations,
				IgnoreEval = IsEvalIgnored,
				ObfuscateJavascript = ObfuscateJavaScript,
				PreserveAllSemicolons = PreserveAllSemicolons,
				ThreadCulture = ThreadCulture
			};
		}

		protected virtual CompressionType GetCompressionTypeFor(IFileSpec fileSpec)
		{
			return fileSpec.CompressionType > CompressionType.Unknown
				? fileSpec.CompressionType
				: CompressionType;
		}
	}
}
