﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml.Linq;

namespace Lekmer.UICompressor
{
	public class SettingXmlParser : ISettingXmlParser
	{
		public virtual ICompressorJobList Parse(string settingXmlSource)
		{
			ICompressorJobList compressorJobList = new CompressorJobList();

			XDocument xmlDocument = XDocument.Parse(settingXmlSource);

			if (xmlDocument.Root == null)
			{
				return compressorJobList;
			}

			IEnumerable<XElement> targetElements = xmlDocument.Root.Elements("Target");

			compressorJobList.CompressorJobs = new List<ICompressorJob>();

			foreach (var targetElement in targetElements)
			{
				string name = ParseAttribute(targetElement, "Name");

				List<IFileSpec> cssFiles = ParseFiles(targetElement, "CssFiles");
				List<IFileSpec> jsFiles = ParseFiles(targetElement, "JavaScriptFiles");

				List<ICssCompressorJob> cssCompressorJobs = ParseCssJobs(targetElement);
				List<IJsCompressorJob> jsCompressorJobs = ParseJsJobs(targetElement);

				ICompressorJob compressorJob = new CompressorJob
				{
					Name = name,
					CssFiles = cssFiles,
					JsFiles = jsFiles,
					CssCompressorJobs = cssCompressorJobs,
					JsCompressorJobs = jsCompressorJobs
				};

				compressorJobList.CompressorJobs.Add(compressorJob);
			}

			return compressorJobList;
		}


		protected virtual List<IFileSpec> ParseFiles(XElement targetElement, string nodeName)
		{
			var files = new List<IFileSpec>();

			XElement itemGroupElement = targetElement.Element("ItemGroup");
			if (itemGroupElement == null)
			{
				return files;
			}

			IEnumerable<XElement> fileElements = itemGroupElement.Elements(nodeName);

			foreach (var fileElement in fileElements)
			{
				string fileName = ParseAttribute(fileElement, "Include");
				CompressionType compressionType = ParseCompressionTypeAttribute(fileElement, "CompressionType");

				files.Add(new FileSpec(fileName, compressionType));
			}

			return files;
		}

		protected virtual List<ICssCompressorJob> ParseCssJobs(XElement targetElement)
		{
			IEnumerable<XElement> jobElements = targetElement.Elements("CssCompressorTask");

			var jobs = new List<ICssCompressorJob>();

			foreach (var jobElement in jobElements)
			{
				string outputFile = ParseAttribute(jobElement, "OutputFile");
				CompressionType compressionType = ParseCompressionTypeAttribute(jobElement, "CompressionType", CompressionType.StandardYui);
				Encoding encodingType = ParseEncodingTypeAttribute(jobElement, "EncodingType");
				int lineBreakPosition = ParseIntAttribute(jobElement, "LineBreakPosition", -1);
				bool preserveComments = ParseBooleanAttribute(jobElement, "PreserveComments");

				ICssCompressorJob cssCompressorJob = new CssCompressorJob
				{
					OutputFile = outputFile,
					CompressionType = compressionType,
					Encoding = encodingType,
					LineBreakPosition = lineBreakPosition,
					PreserveComments = preserveComments
				};

				jobs.Add(cssCompressorJob);
			}

			return jobs;
		}

		protected virtual List<IJsCompressorJob> ParseJsJobs(XElement targetElement)
		{
			IEnumerable<XElement> jobElements = targetElement.Elements("JavaScriptCompressorTask");

			var jobs = new List<IJsCompressorJob>();

			foreach (var jobElement in jobElements)
			{
				string outputFile = ParseAttribute(jobElement, "OutputFile");
				CompressionType compressionType = ParseCompressionTypeAttribute(jobElement, "CompressionType", CompressionType.StandardYui);
				Encoding encodingType = ParseEncodingTypeAttribute(jobElement, "EncodingType");
				int lineBreakPosition = ParseIntAttribute(jobElement, "LineBreakPosition", -1);
				bool obfuscateJavaScript = ParseBooleanAttribute(jobElement, "ObfuscateJavaScript", true);
				bool preserveAllSemicolons = ParseBooleanAttribute(jobElement, "PreserveAllSemicolons");
				bool disableOptimizations = ParseBooleanAttribute(jobElement, "DisableOptimizations");
				CultureInfo threadCulture = ParseThreadCultureAttribute(jobElement, "ThreadCulture");
				bool isEvalIgnored = ParseBooleanAttribute(jobElement, "IsEvalIgnored");

				IJsCompressorJob jsCompressorJob = new JsCompressorJob
				{
					OutputFile = outputFile,
					CompressionType = compressionType,
					Encoding = encodingType,
					LineBreakPosition = lineBreakPosition,
					ObfuscateJavaScript = obfuscateJavaScript,
					PreserveAllSemicolons = preserveAllSemicolons,
					DisableOptimizations = disableOptimizations,
					ThreadCulture = threadCulture,
					IsEvalIgnored = isEvalIgnored
				};

				jobs.Add(jsCompressorJob);
			}

			return jobs;
		}


		protected virtual string ParseAttribute(XElement element, string attributeName, string defaultValue = null)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return defaultValue;
			}

			return attribute.Value;
		}

		protected virtual bool ParseBooleanAttribute(XElement element, string attributeName, bool defaultValue = false)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return defaultValue;
			}

			bool value;
			if (bool.TryParse(attribute.Value, out value))
			{
				return value;
			}

			return defaultValue;
		}

		protected virtual int ParseIntAttribute(XElement element, string attributeName, int defaultValue = 0)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return defaultValue;
			}

			int value;
			if (int.TryParse(attribute.Value, out value))
			{
				return value;
			}

			return defaultValue;
		}

		protected virtual CompressionType ParseCompressionTypeAttribute(XElement element, string attributeName, CompressionType defaultValue = CompressionType.Unknown)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return defaultValue;
			}

			CompressionType value;

			switch (attribute.Value)
			{
				case "None":
					value = CompressionType.None;
					break;

				case "Standard":
					value = CompressionType.StandardYui;
					break;

				default:
					value = defaultValue;
					break;
			}

			return value;
		}

		protected virtual Encoding ParseEncodingTypeAttribute(XElement element, string attributeName)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return Encoding.Default;
			}

			Encoding encodingType;

			switch (attribute.Value.ToLowerInvariant())
			{
				case "ascii":
					encodingType = Encoding.ASCII;
					break;
				case "bigendianunicode":
					encodingType = Encoding.BigEndianUnicode;
					break;
				case "unicode":
					encodingType = Encoding.Unicode;
					break;
				case "utf32":
				case "utf-32":
					encodingType = Encoding.UTF32;
					break;
				case "utf7":
				case "utf-7":
					encodingType = Encoding.UTF7;
					break;
				case "utf8":
				case "utf-8":
					encodingType = Encoding.UTF8;
					break;
				case "default":
					encodingType = Encoding.Default;
					break;
				default:
					throw new ArgumentException("Encoding: " + attribute.Value + " is invalid.");
			}

			return encodingType;
		}

		protected virtual CultureInfo ParseThreadCultureAttribute(XElement element, string attributeName)
		{
			XAttribute attribute = element.Attribute(attributeName);

			if (attribute == null)
			{
				return CultureInfo.InvariantCulture;
			}

			CultureInfo threadCulture;

			try
			{
				switch (attribute.Value.ToLowerInvariant())
				{
					case "iv":
					case "ivl":
					case "invariantculture":
					case "invariant culture":
					case "invariant language":
					case "invariant language (invariant country)":
						{
							threadCulture = CultureInfo.InvariantCulture;
							break;
						}
					default:
						{
							threadCulture = CultureInfo.CreateSpecificCulture(attribute.Value);
							break;
						}
				}
			}
			catch
			{
				throw new ArgumentException("Thread Culture: " + attribute.Value + " is invalid.");
			}

			return threadCulture;
		}
	}
}
