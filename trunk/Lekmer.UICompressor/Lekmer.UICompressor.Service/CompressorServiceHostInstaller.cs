﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Lekmer.UICompressor.Service
{
	[RunInstaller(true)]
	public partial class CompressorServiceHostInstaller : System.Configuration.Install.Installer
	{
		public CompressorServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
