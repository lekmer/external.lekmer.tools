﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using log4net;

namespace Lekmer.UICompressor.Service
{
	public partial class CompressorServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static IFilesGroupWatcher _settingFileWatcher;
		private static ICompressorJobList _compressorJobList;

		public CompressorServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			string settingXmlFileName = ConfigurationManager.AppSettings["SettingXmlFile"];

			_settingFileWatcher = new FilesGroupWatcher(new List<IFileSpec> { new FileSpec(settingXmlFileName, CompressionType.Unknown) });
			_settingFileWatcher.Changed += SettingFileWatcherOnChanged;
			_settingFileWatcher.StartWatching(true);

			_log.Info("Service statred.");
		}

		protected override void OnStop()
		{
			if (_settingFileWatcher != null)
			{
				_settingFileWatcher.Dispose();
			}

			_log.Info("Service stopped.");
		}

		protected virtual void GetThingsDone()
		{
			try
			{
				string settingXmlFileName = ConfigurationManager.AppSettings["SettingXmlFile"];

				// Read setting file
				string settingXmlSource = File.ReadAllText(settingXmlFileName);

				if (_compressorJobList != null)
				{
					_compressorJobList.Dispose();
					_compressorJobList = null;
				}

				ISettingXmlParser settingXmlParser = new SettingXmlParser();
				_compressorJobList = settingXmlParser.Parse(settingXmlSource);

				_compressorJobList.Execute();
			}
			catch (Exception ex)
			{
				_log.Error("Compression failure.", ex);
			}
		}

		protected virtual void SettingFileWatcherOnChanged(object sender, EventArgs eventArgs)
		{
			_log.Info("Change detected to settings file.");
			GetThingsDone();
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Launch();
		}
	}
}
