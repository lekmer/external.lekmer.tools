﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using log4net;

namespace Lekmer.UICompressor.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static IFilesGroupWatcher _settingFileWatcher;
		private static ICompressorJobList _compressorJobList;

		static void Main()
		{
			string settingXmlFileName = ConfigurationManager.AppSettings["SettingXmlFile"];

			GetThingsDone();

			_settingFileWatcher = new FilesGroupWatcher(new List<IFileSpec> { new FileSpec(settingXmlFileName, CompressionType.Unknown) });
			_settingFileWatcher.Changed += SettingFileWatcherOnChanged;
			_settingFileWatcher.StartWatching();

			System.Console.WriteLine("Press \'q\' to quit the sample.");
			while (System.Console.Read() != 'q')
			{
			}
		}

		private static void GetThingsDone()
		{
			string settingXmlFileName = ConfigurationManager.AppSettings["SettingXmlFile"];

			// Read setting file
			string settingXmlSource = File.ReadAllText(settingXmlFileName);

			ISettingXmlParser settingXmlParser = new SettingXmlParser();
			_compressorJobList = settingXmlParser.Parse(settingXmlSource);

			_compressorJobList.Execute();
		}

		private static void SettingFileWatcherOnChanged(object sender, EventArgs eventArgs)
		{
			GetThingsDone();
		}
	}
}
