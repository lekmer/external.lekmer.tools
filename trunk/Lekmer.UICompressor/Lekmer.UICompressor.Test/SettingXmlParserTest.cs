﻿using System.Globalization;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace Lekmer.UICompressor.Test
{
	[TestFixture]
	public class SettingXmlParserTest
	{
		[Test]
		public void Parse_SimpleSettingXmlFile_CorrectResult()
		{
			string settingXmlFileName = @"..\..\TestFiles\Compressor_settings_simple.xml";

			// Read setting file
			string settingXmlSource = File.ReadAllText(settingXmlFileName);

			ISettingXmlParser settingXmlParser = new SettingXmlParser();
			ICompressorJobList compressorJobList = settingXmlParser.Parse(settingXmlSource);

			Assert.IsNotNull(compressorJobList);
			Assert.IsNotNull(compressorJobList.CompressorJobs);
			Assert.AreEqual(1, compressorJobList.CompressorJobs.Count);

			ICompressorJob compressorJob = compressorJobList.CompressorJobs.First();

			Assert.AreEqual("SimpleMinify", compressorJob.Name);
			Assert.IsNotNull(compressorJob.CssFiles);
			Assert.IsNotNull(compressorJob.JsFiles);
			Assert.IsNotNull(compressorJob.CssCompressorJobs);
			Assert.IsNotNull(compressorJob.JsCompressorJobs);
			Assert.AreEqual(4, compressorJob.CssFiles.Count);
			Assert.AreEqual(1, compressorJob.JsFiles.Count);
			Assert.AreEqual(1, compressorJob.CssCompressorJobs.Count);
			Assert.AreEqual(1, compressorJob.JsCompressorJobs.Count);

			for (int i = 0; i < compressorJob.CssFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob.CssFiles[i];

				Assert.AreEqual(string.Format("StylesheetSample{0}.css", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.Unknown, fileSpec.CompressionType);
			}

			for (int i = 0; i < compressorJob.JsFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob.JsFiles[i];

				Assert.AreEqual(string.Format("JavaScriptSample{0}.js", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.Unknown, fileSpec.CompressionType);
			}

			ICssCompressorJob cssCompressorJob = compressorJob.CssCompressorJobs.First();
			IJsCompressorJob jsCompressorJob = compressorJob.JsCompressorJobs.First();

			Assert.AreEqual("Minified.css", cssCompressorJob.OutputFile);
			Assert.AreEqual(CompressionType.StandardYui, cssCompressorJob.CompressionType);
			Assert.AreEqual(-1, cssCompressorJob.LineBreakPosition);
			Assert.AreEqual(false, cssCompressorJob.PreserveComments);

			Assert.AreEqual("Minified.js", jsCompressorJob.OutputFile);
			Assert.AreEqual(CompressionType.StandardYui, jsCompressorJob.CompressionType);
			Assert.AreEqual(-1, jsCompressorJob.LineBreakPosition);
			Assert.AreEqual(true, jsCompressorJob.ObfuscateJavaScript);
			Assert.AreEqual(false, jsCompressorJob.PreserveAllSemicolons);
			Assert.AreEqual(false, jsCompressorJob.DisableOptimizations);
			Assert.AreEqual(CultureInfo.InvariantCulture, jsCompressorJob.ThreadCulture);
			Assert.AreEqual(false, jsCompressorJob.IsEvalIgnored);

		}

		[Test]
		public void Parse_RegularSettingXmlFile_CorrectResult()
		{
			string settingXmlFileName = @"..\..\TestFiles\Compressor_settings_regular.xml";

			// Read setting file
			string settingXmlSource = File.ReadAllText(settingXmlFileName);

			ISettingXmlParser settingXmlParser = new SettingXmlParser();
			ICompressorJobList compressorJobList = settingXmlParser.Parse(settingXmlSource);

			Assert.IsNotNull(compressorJobList);
			Assert.IsNotNull(compressorJobList.CompressorJobs);
			Assert.AreEqual(1, compressorJobList.CompressorJobs.Count);

			ICompressorJob compressorJob = compressorJobList.CompressorJobs.First();

			Assert.AreEqual("RegularMinify", compressorJob.Name);
			Assert.IsNotNull(compressorJob.CssFiles);
			Assert.IsNotNull(compressorJob.JsFiles);
			Assert.IsNotNull(compressorJob.CssCompressorJobs);
			Assert.IsNotNull(compressorJob.JsCompressorJobs);
			Assert.AreEqual(4, compressorJob.CssFiles.Count);
			Assert.AreEqual(1, compressorJob.JsFiles.Count);
			Assert.AreEqual(1, compressorJob.CssCompressorJobs.Count);
			Assert.AreEqual(1, compressorJob.JsCompressorJobs.Count);

			for (int i = 0; i < compressorJob.CssFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob.CssFiles[i];

				Assert.AreEqual(string.Format("StylesheetSample{0}.css", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.StandardYui, fileSpec.CompressionType);
			}

			for (int i = 0; i < compressorJob.JsFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob.JsFiles[i];

				Assert.AreEqual(string.Format("JavaScriptSample{0}.js", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.StandardYui, fileSpec.CompressionType);
			}

			ICssCompressorJob cssCompressorJob = compressorJob.CssCompressorJobs.First();
			IJsCompressorJob jsCompressorJob = compressorJob.JsCompressorJobs.First();

			Assert.AreEqual("Minified.css", cssCompressorJob.OutputFile);
			Assert.AreEqual(CompressionType.None, cssCompressorJob.CompressionType);
			Assert.AreEqual(800, cssCompressorJob.LineBreakPosition);
			Assert.AreEqual(true, cssCompressorJob.PreserveComments);

			Assert.AreEqual("Minified.js", jsCompressorJob.OutputFile);
			Assert.AreEqual(CompressionType.None, jsCompressorJob.CompressionType);
			Assert.AreEqual(800, jsCompressorJob.LineBreakPosition);
			Assert.AreEqual(false, jsCompressorJob.ObfuscateJavaScript);
			Assert.AreEqual(true, jsCompressorJob.PreserveAllSemicolons);
			Assert.AreEqual(true, jsCompressorJob.DisableOptimizations);
			Assert.AreEqual("sv-SE", jsCompressorJob.ThreadCulture.Name);
			Assert.AreEqual(true, jsCompressorJob.IsEvalIgnored);

		}

		[Test]
		public void Parse_MultipleSettingXmlFile_CorrectResult()
		{
			string settingXmlFileName = @"..\..\TestFiles\Compressor_settings_multiple.xml";

			// Read setting file
			string settingXmlSource = File.ReadAllText(settingXmlFileName);

			ISettingXmlParser settingXmlParser = new SettingXmlParser();
			ICompressorJobList compressorJobList = settingXmlParser.Parse(settingXmlSource);

			Assert.IsNotNull(compressorJobList);
			Assert.IsNotNull(compressorJobList.CompressorJobs);
			Assert.AreEqual(2, compressorJobList.CompressorJobs.Count);

			ICompressorJob compressorJob1 = compressorJobList.CompressorJobs[0];
			ICompressorJob compressorJob2 = compressorJobList.CompressorJobs[1];

			Assert.AreEqual("MultipleCssMinify", compressorJob1.Name);
			Assert.IsNotNull(compressorJob1.CssFiles);
			Assert.IsNotNull(compressorJob1.JsFiles);
			Assert.IsNotNull(compressorJob1.CssCompressorJobs);
			Assert.IsNotNull(compressorJob1.JsCompressorJobs);
			Assert.AreEqual(4, compressorJob1.CssFiles.Count);
			Assert.AreEqual(0, compressorJob1.JsFiles.Count);
			Assert.AreEqual(2, compressorJob1.CssCompressorJobs.Count);
			Assert.AreEqual(0, compressorJob1.JsCompressorJobs.Count);

			Assert.AreEqual("MultipleJsMinify", compressorJob2.Name);
			Assert.IsNotNull(compressorJob2.CssFiles);
			Assert.IsNotNull(compressorJob2.JsFiles);
			Assert.IsNotNull(compressorJob2.CssCompressorJobs);
			Assert.IsNotNull(compressorJob2.JsCompressorJobs);
			Assert.AreEqual(0, compressorJob2.CssFiles.Count);
			Assert.AreEqual(4, compressorJob2.JsFiles.Count);
			Assert.AreEqual(0, compressorJob2.CssCompressorJobs.Count);
			Assert.AreEqual(2, compressorJob2.JsCompressorJobs.Count);

			for (int i = 0; i < compressorJob1.CssFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob1.CssFiles[i];

				Assert.AreEqual(string.Format("StylesheetSample{0}.css", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.Unknown, fileSpec.CompressionType);
			}

			for (int i = 0; i < compressorJob2.JsFiles.Count; i++)
			{
				IFileSpec fileSpec = compressorJob2.JsFiles[i];

				Assert.AreEqual(string.Format("JavaScriptSample{0}.js", i + 1), fileSpec.FileName);
				Assert.AreEqual(CompressionType.Unknown, fileSpec.CompressionType);
			}

			ICssCompressorJob cssCompressorJob1 = compressorJob1.CssCompressorJobs[0];
			ICssCompressorJob cssCompressorJob2 = compressorJob1.CssCompressorJobs[1];
			IJsCompressorJob jsCompressorJob1 = compressorJob2.JsCompressorJobs[0];
			IJsCompressorJob jsCompressorJob2 = compressorJob2.JsCompressorJobs[1];

			Assert.AreEqual("App.css", cssCompressorJob1.OutputFile);
			Assert.AreEqual(CompressionType.None, cssCompressorJob1.CompressionType);

			Assert.AreEqual("App.min.css", cssCompressorJob2.OutputFile);
			Assert.AreEqual(CompressionType.StandardYui, cssCompressorJob2.CompressionType);

			Assert.AreEqual("App.js", jsCompressorJob1.OutputFile);
			Assert.AreEqual(CompressionType.None, jsCompressorJob1.CompressionType);

			Assert.AreEqual("App.min.js", jsCompressorJob2.OutputFile);
			Assert.AreEqual(CompressionType.StandardYui, jsCompressorJob2.CompressionType);
		}
	}
}
